package techplayground.projectsugarman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSugarmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectSugarmanApplication.class, args);
	}

}
